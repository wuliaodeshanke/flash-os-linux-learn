#include <linux/mm.h>

// 实际物理内存最高端地址
unsigned long HIGH_MEMORY = 0;
// 内存映射字节图(1字节代表1页物理内存的使用情况)
unsigned char mem_map[PAGING_PAGES] = { 0, };

void mem_init(long start_mem, long end_mem) {
	int i;
	// 将1-16M的内存标记为USED（100）已占用状态
	HIGH_MEMORY = end_mem;
	for (i = 0; i < PAGING_PAGES; i++) {
		mem_map[i] = USED;
	}
	// 找到物理内存区起始的页面号
	i = MAP_NR(start_mem);
	// 得到主内存区的页面数量
	long main_mem_pages = (end_mem - start_mem) >> 12;
	// 将主内存区对应的页面的使用数量置为0（未使用）之后系统只将为 0 的视为空闲页面
	while (main_mem_pages --> 0) {
		mem_map[i++] = 0;
	}
}