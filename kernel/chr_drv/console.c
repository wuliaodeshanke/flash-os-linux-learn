#include <asm/io.h>
#include <linux/tty.h>

/**获取系统初始化所需要的参数;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 内存地址; 字节; 内容;
; 0x90000; 2;   光标位置;
; 0x90002; 2;	扩展内存大小;
; 0x90004; 2;	显示页面;
; 0x90006; 1;	显示模式;
; 0x90007; 1;	字符列数;
; 0x90008; 2; ? ? ;
; 0x9000A; 1;	安装的显示内存;
; 0x9000B; 1;	显示状态(彩色 / 单色);
; 0x9000C; 2;	显示卡特性参数;
; 0x9000E; 1;	屏幕当前行值;
; 0x9000F; 1;	屏幕当前列值;
; ...;
; 0x90080; 16;	第1个硬盘的参数表;
; 0x90090; 16;	第2个硬盘的参数表;
; 0x901FC; 2;	根文件系统所在的设备号（bootsec.s中设置）;
*/

#define ORIG_X				(*(unsigned char *)0x90000)
#define ORIG_Y				(*(unsigned char *)0x90001)
#define ORIG_VIDEO_PAGE		(*(unsigned short *)0x90004)
#define ORIG_VIDEO_MODE		((*(unsigned short *)0x90006) & 0xff)
#define ORIG_VIDEO_COLS 	(((*(unsigned short *)0x90006) & 0xff00) >> 8)
#define ORIG_VIDEO_LINES	((*(unsigned short *)0x9000e) & 0xff)
#define ORIG_VIDEO_EGA_AX	(*(unsigned short *)0x90008)
#define ORIG_VIDEO_EGA_BX	(*(unsigned short *)0x9000a)
#define ORIG_VIDEO_EGA_CX	(*(unsigned short *)0x9000c)

static unsigned long	video_num_columns;	// number of text columns
static unsigned long	video_num_lines;	// number of text lines
static unsigned long	video_size_row;		// bytes per row
static unsigned long	video_mem_base;		// Base of video memory
static unsigned long	video_port_reg;		// video register select port
static unsigned long	video_port_val;		// video register value port

static struct {
	unsigned short	vc_video_erase_char;
	unsigned char	vc_attr;
	unsigned char	vc_def_attr;
	int				vc_bold_attr;
	unsigned long	vc_ques;
	unsigned long	vc_state;
	unsigned long	vc_restate;
	unsigned long	vc_checkin;
	unsigned long	vc_origin;			/* Used for EGA/VGA fast scroll	*/
	unsigned long	vc_scr_end;			/* Used for EGA/VGA fast scroll	*/
	unsigned long	vc_pos;
	unsigned long	vc_x, vc_y;
	unsigned long	vc_top, vc_bottom;
	//unsigned long	vc_npar, vc_par[NPAR];
	unsigned long	vc_video_mem_start;
	unsigned long	vc_video_mem_end;
	unsigned int	vc_saved_x;
	unsigned int	vc_saved_y;
	unsigned int	vc_iscolor;
	char* vc_translate;
} vc_cons[MAX_CONSOLES];

#define origin			(vc_cons[currcons].vc_origin)
#define pos				(vc_cons[currcons].vc_pos)
#define top				(vc_cons[currcons].vc_top)
#define bottom			(vc_cons[currcons].vc_bottom)
#define x				(vc_cons[currcons].vc_x)
#define y				(vc_cons[currcons].vc_y)
#define attr			(vc_cons[currcons].vc_attr)
#define video_mem_start	(vc_cons[currcons].vc_video_mem_start)

static inline void gotoxy(int currcons, int new_x, unsigned int new_y) {
	if (new_x > video_num_columns || new_y >= video_num_columns) {
		return;
	}
	x = new_x;
	y = new_y;
	pos = origin + y * video_size_row + (x << 1);
}

static inline void set_cursor(int currcons) {
	outb_p(14, video_port_reg);
	outb_p(0xff & ((pos - video_mem_base) >> 9), video_port_val);
	outb_p(15, video_port_reg);
	outb_p(0xff & ((pos - video_mem_base) >> 1), video_port_val);
}

static char buf[1024];

/* 初始化 */
void con_init(void) {
	int currcons = 0;
	video_num_columns = ORIG_VIDEO_COLS;
	video_num_lines = ORIG_VIDEO_LINES;
	//video_size_row = video_num_columns * 2;
	//bottom = video_num_lines;
	video_size_row = 160;
	bottom = 25;
	top = 0;

	if (ORIG_VIDEO_MODE == 7) {
		// 黑白显卡模式
		video_mem_base = 0xb0000;
		video_port_reg = 0x3b4;
		video_port_val = 0x3b5;
	} else {
		// 彩色模式
		video_mem_base = 0xb8000;
		video_port_reg = 0x3b4;
		video_port_val = 0x3b5;
	}

	origin = video_mem_start = video_mem_base;
	//gotoxy(currcons, ORIG_X, ORIG_Y);
	gotoxy(currcons, 0, 5);
	attr = 0x07;

	for (int i = 0; i < 20; i++) {
		buf[i] = 'a' + i;
	};
	console_print(buf);
	
}

static void scrup(int currcons) {

}

/* 归位行首 */
static void cr(int currcons)
{
	pos -= x << 1;
	x = 0;
}

/* 换行 */
static void lf(int currcons)
{
	if (y + 1 < bottom) {
		y++;
		pos += video_size_row;
		return;
	}
	scrup(currcons);
}

void console_print(const char b[]) {
	char c;
	while ((c = *(b++))) {
		console_put_char(c);
	}
}

void console_put_char(char c) {
	int currcons = fg_console;
	char* vram = (char*)(pos);

	// ls换行符 \n
	if (c == 10) {
		cr(currcons);
		lf(currcons);
		return;
	}
	// cr归位键 \r
	if (c == 13) {
		cr(currcons);
		return;
	}
	// 自动换行
	if (x >= video_num_columns) {
		x -= video_num_columns;
		pos -= video_size_row;
		lf(currcons);
	}

	*vram++ = c;
	*vram++ = 0x07;

	pos += 2;
	x++;

	// 刷新光标
	set_cursor(currcons);
}

void console_put_hex(int num) {
	int currcons = fg_console;
	char* vram = (char*)(pos);

	// 自动换行
	if (x >= video_num_columns) {
		x -= video_num_columns;
		pos -= video_size_row;
		lf(currcons);
	}

	console_put_char('0');
	console_put_char('x');
	console_put_char('0');

    int start_not_zero = 0;

	for (int i = 7; i >= 0; i--)
	{
		int cur_index_num = ((num >> (i * 4)) & 0x0000000f);
		char c;
		if (cur_index_num != 0) {
			start_not_zero = 1;
		}
		if (cur_index_num < 10) {
			c = (char)(cur_index_num) + 0x30;
		} else {
			c = (char)(cur_index_num) + 0x57;
		}
		
		if (start_not_zero) {
			console_put_char(c);
		}
		
	}

	console_put_char(' ');

	// 刷新光标
	set_cursor(currcons);
}

void console_put_hex_ln(int num) {
	console_put_hex(num);
	console_put_char(10);
	console_put_char(13);
}