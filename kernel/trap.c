#include <asm/system.h>

extern void console_put_hex(int);

void divide_error(void);

/**
 * 异常（陷阱）中断程序初始化
 * 有两个函数 set_trap_gate 和 set_system_gate 均是使用中断描述符表的陷阱门
 * 区别是前者特权级0，后者为3，3就是任何程序都能访问了
 */
void trap_init() {
	int i;
	set_intr_gate(1, &divide_error);

}