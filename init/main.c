#define EXT_MEM_K 15 //扩展内存大小16-1=15KB

/* 内存管理初始化 mm/memory.c */
extern void mem_init(long start, long end);
/* 陷阱门初始化 kernel/trap.c */
extern void trap_init(void);
/* tty初始化 kernel/chr_drv/console.c */
extern void con_init(void);

// 物理内存总量
static long memory_end = 0;
// 高速缓冲区末端地址
static long buffer_memory_end = 0;
// 主内存开始位置
static long main_memory_start = 0;

// 为了证明确实执行到此处特意设置的无效值
static long count = 0;

int kernel_start() {
	/* linux是根据物理内存容量动态设置内存分布，我们这里直接当内存为16M处理 */
	memory_end = (1 << 20) + (EXT_MEM_K << 10); // 总内存16M
	buffer_memory_end = 4 * 1024 * 1024;		// 4M之前为缓冲区
	main_memory_start = buffer_memory_end;		// 4M之后为主内存

	/* 以下为所有初始化工作，写的还挺规整的 */
	mem_init(main_memory_start, memory_end);	// 主内存区初始化
	con_init();									// tty初始化
	trap_init();								// 陷阱门初始化

	// 系统怠速
	for (;;) {
		count++;
	}
}