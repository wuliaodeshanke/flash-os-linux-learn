include Rules.make

CHR = kernel/chr_drv

all: Image

Image: boot/bootsect.bin boot/setup.bin boot/head.bin init/main.bin
	del /q others\bochs\os.raw
	bximage -mode=create -hd=60 -q $(BOCHS_HOME)/os.raw
	dd if=boot/bootsect.bin of=$(BOCHS_HOME)/os.raw bs=512 count=1
	dd if=boot/setup.bin of=$(BOCHS_HOME)/os.raw bs=512 count=4 seek=1
	dd if=boot/head.bin of=$(BOCHS_HOME)/os.raw bs=512 count=48 seek=5
	dd if=init/main.bin of=$(BOCHS_HOME)/os.raw bs=512 count=100 seek=53
	
boot/bootsect.bin: boot/bootsect.s
	nasm -I include/ -o boot/bootsect.bin boot/bootsect.s -l boot/bootsect.lst
	
boot/setup.bin: boot/setup.s
	nasm -I include/ -o boot/setup.bin boot/setup.s -l boot/setup.lst
	
boot/head.bin: boot/head.s
	nasm -I include/ -o boot/head.bin boot/head.s -l boot/head.lst
	
init/main.bin: init/main.c mm/mm.o kernel/kernel.o $(CHR)/tty.o
	gcc -c -fno-builtin -o init/main.o init/main.c
	$(LD) init/main.o mm/mm.o kernel/kernel.o $(CHR)/tty.o -Ttext 0x5000 -e _kernel_start -o init/main.bin
	
tools/build:

run: Image
	bochs -f $(BOCHS_HOME)/bochsrc.disk -q
	
brun: Image
	bochsdbg -f $(BOCHS_HOME)/bochsrc.disk -q
	
clean:
	del /q others\bochs\os.raw
	del /q boot\*.lst
	del /q boot\*.bin
	del /q boot\*.o
	del /q init\*.bin
	del /q init\*.bin.large
	del /q init\*.o
	del /q mm\*.o

mm/mm.o: mm/memory.c
	gcc -c -fno-builtin -I include -o mm/memory.o mm/memory.c
	$(LD) mm/memory.o -r -o mm/mm.o

kernel/kernel.o: kernel/trap.c kernel/asm.s
	gcc -c -fno-builtin -I include -o kernel/trap.o kernel/trap.c
	nasm -f elf -o kernel/asm.o kernel/asm.s
	$(LD) kernel/trap.o kernel/asm.o -r -o kernel/kernel.o

$(CHR)/tty.o: $(CHR)/console.c $(CHR)/tty_io.c
	gcc -c -fno-builtin -I include -o $(CHR)/console.o $(CHR)/console.c
	gcc -c -fno-builtin -I include -o $(CHR)/tty_io.o $(CHR)/tty_io.c
	$(LD) $(CHR)/console.o $(CHR)/tty_io.o -r -o $(CHR)/tty.o