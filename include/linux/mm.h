#define LOW_MEM 0x100000					// 物理内存低端1MB
#define PAGE_SIZE 4096						// 1页大小
#define PAGING_MEMORY (15*1024*1024)		// 可分页的物理内存大小
#define PAGING_PAGES (PAGING_MEMORY>>12)	// 可分页的物理内存页面数
#define USED 100							// 物理内存被占用

#define MAP_NR(addr) (((addr)-LOW_MEM)>>12)	// 将物理内存地址映射成物理内存页面号（就是去掉页内偏移后的前20位）