#define sti() __asm__ ("sti"::)			/* 开中断 */
#define cli() __asm__ ("cli"::)			/* 关中断 */
#define nop() __asm__ ("nop"::)			/* 空操作 */
#define iret() __asm__ ("iret"::)		/* 中断返回 */

#define gdt (*(unsigned int *)0x91000)
#define idt (*(unsigned int *)0x91004)
#define pg_dir (*(unsigned int *)0x91008)

/**
 * 设置门描述符宏
 * @param[in]	gate_addr	在中断描述符表中的偏移量
 * @param[in]	type		门描述符类型
 * @param[in]	dpl			特权级信息
 * @param[in]	addr		中断或异常过程函数地址
 */
#define _set_gate(gate_addr, type, dpl, addr)				\
	__asm__ ("movw %%dx,%%ax\n\t"							\
			"movw %0,%%dx\n\t"								\
			"movl %%eax,%1\n\t"								\
			"movl %%edx,%2"									\
			:												\
			: "i" ((short)(0x8000+(dpl<<13)+(type<<8))),	\
			  "o" (*((char*)(gate_addr))),					\
			  "o" (*(4+(char*)(gate_addr))),				\
			  "d" ((char*)(addr)),"a"(0x00080000))

/**
 * 中断门
 */
#define set_intr_gate(n, addr) _set_gate(idt + n*8, 14, 0, addr)

/**
 * 陷阱门
 */
#define set_trap_gate(n, addr) _set_gate(&idt[n], 15, 0, addr)

/**
 * 系统陷阱门
 */
#define set_system_gate(n, addr) _set_gate(&idt[n], 15, 3, addr)